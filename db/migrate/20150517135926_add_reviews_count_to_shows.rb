class AddReviewsCountToShows < ActiveRecord::Migration
  def up
    add_column :shows, :reviews_count, :integer

    Show.connection.execute <<-SQL
      UPDATE shows
      SET reviews_count =
        (SELECT COUNT(*)
         FROM reviews
         WHERE show_id = shows.id)
    SQL
  end

  def down
    remove_column :shows, :reviews_count
  end
end

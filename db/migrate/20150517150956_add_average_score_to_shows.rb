class AddAverageScoreToShows < ActiveRecord::Migration
  def up
    add_column :shows, :average_score, :float

    Show.connection.execute <<-SQL
      UPDATE shows
      SET average_score =
        (SELECT AVG(score)
         FROM reviews
         WHERE show_id = shows.id)
    SQL

    add_index :shows, :average_score
  end

  def down
    remove_index :shows, :average_score
    remove_column :shows, :average_score
  end
end

class Show < ActiveRecord::Base
  has_many :reviews
  after_touch :sync_average_score

  private

  # It should have been gone to background jobs instead but keeping it for simpler implementation
  def sync_average_score
    self.update_column(:average_score, self.reviews.average(:score))
  end
end

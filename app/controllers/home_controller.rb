class HomeController < ApplicationController
  def leaderboard
    @shows = Show.select(:title, :average_score, :reviews_count).order(average_score: :desc).limit(10)
  end
end
